var requestSRC = 'https://api.openweathermap.org/data/2.5/weather?q=Toulouse,fr&appid=c21a75b667d6f7abb81f118dcf8d4611&units=metric';

var request = new XMLHttpRequest();
request.open('GET', requestSRC);
request.send();

request.onload = function() {
    var jsonObj = JSON.parse(request.responseText);
    console.log(jsonObj);

    var template = `
    <div class="Ville">
        <h3>{{name}}</h3>
        <p>Code : {{cod}}</p>
        <p>Pays : {{sys.country}}</p>
    </div>
    <div class="coordonees">
        <h3>Coordonnées</h3>
        <p>Longitude : {{coord.lon}}</p>
        <p>Latitude : {{coord.lat}}</p>
    </div>
    <div class="main">
        <h3>Général</h3>
        <p>Température : {{main.temp}}°C</p>
        <p>Température min : {{main.temp_min}}°C</p>
        <p>Température max : {{main.temp_max}}°C</p>
        <p>Taux d'humidité : {{main.humidity}}%</p>
        <p>Pression : {{main.pressure}}Pa</p>

    </div>
    `

        Mustache.parse();
        var rendered = Mustache.render(template, jsonObj);
        $("#app").append(rendered);
}

